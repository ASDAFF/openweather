# openweather

## Компонент погоды по api openweather на сайте для 1С-Битрикс

Клонируйте компонент в свое пространство имен

Компонент находится в разделе компонентов "Контент" в визуальном редакторе.

При необходимости привести шаблон компонента к дизайну сайта

Пример подключения
```php
$APPLICATION->IncludeComponent(
	"{{Ваше пространство имен}}:openweather",
	"",
	Array(
		"API_KEY" => "{{ключ api}}",
		"LATITUDE" => "52.0975500", // широта
		"LONGITUDE" => "23.6877500" // долгота
	)
);
```

![демонстрация работы](https://gitlab.com/dimabresky/openweather/raw/dev/img.jpg)

Для вывода требудется наличие api key. Получить его можно зарегистрировавшись на https://home.openweathermap.org/api_keys

Тарифы https://openweathermap.org/price
