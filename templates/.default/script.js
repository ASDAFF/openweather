/* 
 * @package openweather
 * @author ИП Бреский Дмитрий Игоревич <dimabresky@gmail.com>
 */


if (!BX.OpenWeather) {
    
    BX.namespace('YandexWeather');
    
    BX.OpenWeather = function (options) {
        
        options.data.sessid = BX.bitrix_sessid();
        BX.ajax.post(options.url, options.data, function (resp) {
            
            if (typeof resp === "string" && resp.length) {
                
                BX(options.component_id).innerHTML = resp;
                
            }
            
        });
        
    };
}



