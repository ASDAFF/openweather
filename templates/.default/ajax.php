<?php

define("STOP_STATISTICS", true);
define("NO_KEEP_STATISTIC", "Y");
define("NO_AGENT_STATISTIC", "Y");
define("DisableEventsCheck", true);
define("BX_SECURITY_SHOW_MESSAGE", true);

$documentRoot = filter_input(INPUT_SERVER, 'DOCUMENT_ROOT');
require_once($documentRoot . '/bitrix/modules/main/include/prolog_before.php');

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

$APPLICATION->RestartBuffer();
$APPLICATION->IncludeComponent('travelsoft:openweather', '', [
    "IS_AJAX" => $request->isPost() && $request->isAjaxRequest() && check_bitrix_sessid() ? "Y" : "N",
    "API_KEY" => $_SESSION['OPENWEATHER_BITRIX_COMPONENT_API_KEY'],
    "LATITUDE" => $request->getPost('lat'),
    "LONGITUDE" => $request->getPost('lon')
]);

die();