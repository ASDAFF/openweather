
<div class="openweather" id="<?= $arParams["COMPONENT_ID"] ?>">

    <?= GetMessage('OPENWEATHER_LOADING_TITLE') ?>

</div>

<script>
    new BX.OpenWeather({
        url: '<?= $templateFolder ?>/ajax.php',
        component_id: "<?= $arParams["COMPONENT_ID"] ?>",
        data: {
            lat: <?= $arParams['LATITUDE'] ?>,
            lon: <?= $arParams['LONGITUDE'] ?>,
        }

    });
</script>