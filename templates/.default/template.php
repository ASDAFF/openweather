<?php

if ($arParams["IS_AJAX"]) {
    require 'parts/weather_content.php';
} else {
    require 'parts/weather_wrapper.php';
}