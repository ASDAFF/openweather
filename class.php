<?php

/**
 * @package openweather
 * @author ИП Бреский Дмитрий Игоревич <dimabresky@gmail.com>
 */
class OpenweatherComponent extends CBitrixComponent {

    public function prepareParameters() {

        if (!$this->arParams["API_KEY"]) {
            throw new Exception("yandex weather: api key not found.");
        }
        
        $_SESSION['OPENWEATHER_BITRIX_COMPONENT_API_KEY'] = $this->arParams["API_KEY"];

        if (!$this->arParams["LATITUDE"]) {
            throw new Exception("yandex weather: latitude not found.");
        }

        if (!$this->arParams["LONGITUDE"]) {
            throw new Exception("yandex weather: longitude not found.");
        }

        $this->arParams["IS_AJAX"] = $this->arParams["IS_AJAX"] === "Y";

        $this->arParams["COMPONENT_ID"] = "openweather-" . md5($this->arParams["API_KEY"] . $this->arParams["LATITUDE"] . $this->arParams["LONGITUDE"]);

        CJSCore::Init();
    }

    public function executeComponent() {

        try {

            $this->prepareParameters();

            if ($this->arParams["IS_AJAX"]) {
                $this->arResult["WEATHER_DATA"] = $this->getWeatherData();
            }

            $this->includeComponentTemplate();
        } catch (Exception $ex) {
            ShowError($ex->getMessage());
        }
    }

    /**
     * @return array
     */
    public function getWeatherData() {

        $cache = Bitrix\Main\Data\Cache::createInstance();

        $result = [];
        if ($cache->initCache(3600, $this->arParams["COMPONENT_ID"] . "_" . LANGUAGE_ID)) {

            $result = $cache->getVars();
        } elseif ($cache->startDataCache()) {

            $http_client = new \Bitrix\Main\Web\HttpClient();

            $query_params = [
                'appid' => $this->arParams['API_KEY'],
                'lat' => $this->arParams["LATITUDE"],
                'lon' => $this->arParams["LONGITUDE"],
                'lang' => LANGUAGE_ID,
                'units' => 'metric'
            ];

            $response = $http_client->get('https://api.openweathermap.org/data/2.5/weather?' . http_build_query($query_params));
            $result = $this->parseWeatherResponse($response);

            if (empty($result)) {
                $cache->abortDataCache();
            } else {
                $cache->endDataCache($result);
            }
        }

        return $result;
    }

    /**
     * @param string $response json string
     * @return array
     */
    public function parseWeatherResponse($response) {

        $response = json_decode($response, true);

        $result = [];
        if ($response['weather'][0]) {

            $result['CONDITION'] = $response['weather'][0]['description'];
            $result['ICON'] = $response['weather'][0]['icon'];
            $result['TEMP'] = $response['main']['temp'];
            $result['HUMIDITY'] = $response['main']['humidity'];
            $result['WIND'] = "{$this->getWindDirection($response['wind']['deg'])}, {$response['wind']['speed']}";
            $result['SOURCE'] = $response;
        }

        return $result;
    }

    /**
     * @param int $deg
     * @return string
     */
    public function getWindDirection($deg) {

        if ($deg >= 0 && $deg <= 90) {
            return "NE";
        } elseif ($deg > 90 && $deg <= 180) {
            return "SE";
        } elseif ($deg > 180 && $deg <= 270) {
            return "SW";
        } elseif ($deg > 270 && $deg <= 360) {
            return "NW";
        }

        return '';
    }

}
