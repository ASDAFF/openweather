<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arComponentParameters["PARAMETERS"]['API_KEY'] = array(
    "PARENT" => "BASE",
    "NAME" => "API Ключ",
    "TYPE" => "STRING"
);

$arComponentParameters["PARAMETERS"]['LATITUDE'] = array(
    "PARENT" => "BASE",
    "NAME" => "Широта",
    "TYPE" => "STRING"
);

$arComponentParameters["PARAMETERS"]['LONGITUDE'] = array(
    "PARENT" => "BASE",
    "NAME" => "Долгота",
    "TYPE" => "STRING"
);
